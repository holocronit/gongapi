<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caps extends Model
{
    
    protected $table = 'caps';


    public function services(){
		return $this->hasOne('App\Services','id', 'service_id');

    }

    public function user(){
		return $this->hasOne('App\User','id');

    }
    
}

