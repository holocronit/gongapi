<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\responseProvider;
// use JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;

use App\User as User;
use App\Role as Role;
use App\Session as Session;
use App\Services as Services;
use App\Caps as Caps;


class AuthenticateController extends Controller
{ 


	/*----------  Authenticate function (LOGIN)  ----------*/


	public function authenticate(Request $request) { 
	// prende dalla richiesta le credenziali di accesso
		
		$r = new responseProvider();

		
		$credentials = $request->only('email', 'pwd');


		// VALIDO EMAIL PORCO CANE


		if( empty( $request->email ) || empty( $request->pwd ) ) {
			$r->inError(100);
		}else{
			//search user if it isn't empty
			$user = User::where('email',$request->email)->get()->first();

			if ( empty( $user ) ) { 
			//if user dowsn't exist
				$r->inError(101);
			}else {
				//check if user password is the same of the password passed in the form
				if( $user->password != md5( $request->pwd ) ) {
					//dd(md5($request->pwd));
					$r->inError(102);
				} else {
				//if user exists and password is the same create token, with some info

					$userResponse['user'] = $user->attributesToArray();

					$userResponse['role'] = $user->role()->get();

					$capabilities = Caps::with(['services'])->where('user_id',$user->id)->get();

					$userResponse['capabilities'] = $capabilities;

					$r->addData ($userResponse);

					$expirationTime = AuthenticateController::createValidUntilToken();

					$token = $r->generate_token( $user->id , $expirationTime , $userResponse );

					if ( empty($token) ) {
					//if token empty get error
						$r->inError(103);
					} else {
					//add token in the JWT object, create date and add it to session table
						$r->addToken( $token );

						$r->addData( $token , 'token');

						static::registerTokenForUser( $user->id, $r->token, $expirationTime );

					}
				}

			}

		}

		return response()->json( $r->getResponseData() ); 	
	}


	/*----------  Deauthenticate function (LOGOUT)  ----------*/

	public function deauthenticate(Request $request) {
		
		$r = new responseProvider();

		$session = Session::where('token', $request->token)->get()->first();
		
		if( empty($session) ||  !$session->delete() ){
			$r->inError(110);
		}

		else {
			$session->delete();
		}
		
		return response()->json( $r->getResponseData() );

	}



	/*----------  data function to create valid until timestamp  ----------*/
	


	public function createValidUntilToken ( ) {

		$creationTime = date('Y-m-d H:i:s');

		$updatedValue = strtotime($creationTime) + 60*60;

		$expirationTime = date('Y-m-d H:i:s', $updatedValue);

		return $expirationTime;


	}


	/*----------  register token function for the serviceProvider object  ----------*/



	private static function registerTokenForUser ( $userID, $token, $expirationTime ) {

		$session = new Session;

		$session->user_id = $userID;
		$session->token = $token;
		$session->device = 'desktop';
		$session->validUntil = $expirationTime;
		$session->save();

	}

}