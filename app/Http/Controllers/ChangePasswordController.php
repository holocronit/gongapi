<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\responseProvider;

use App\User as User;
use App\Role as Role;
use App\Session as Session;
use App\Services as Services;
use App\Caps as Caps;

class ChangePasswordController extends Controller
{



    public function update(Request $request) {

    	$r = new responseProvider();

		if( empty( $request->oldPwd ) ) {
			$r->inError(120);
		} else {

			// dd($request->headers);

    		$user = User::where('email',$request->email)->get()->first();



			if ( empty( $user ) ) { 
			//if user doesn't exist
				$r->inError(121);
			} else {
			//check if user password is the same of the password passed in the form
				if( $user->password != md5( $request->oldPwd ) ) {
					$r->inError(122);
				} else {
					//if user exists and password is the same check for changing password
					if ( empty( $request->newPwd ) ) {
						$r->inError(123);
					} else {
						 if ( !empty( $request->newPwd ) ) {
						 	if ( $request->newPwd != $request->newPwdConfirm ) {
						 		$r->inError(124);
						 	}
						 	else {
						 	$changeEff = static::changePwd ( $request->oldEmail, $request->newPwd );
						 	$r->pwdUpdated($changeEff);
						 	}
						 }
					}					
				}
			}
		}

		return response()->json( $r->getUpdateData() );
	}


/**

    TODO:
    - Validate email ------------------> DEVI FARLA PORCODIO
    - Check PWD security

 */


    // private static function changeEmail ( $oldEmail , $newUserMail ) {

    // 	$user = User::where('email',$oldEmail)->get()->first();

    // 	if( $user->email == $newUserMail ) { return 0; }

    // 	$user->email = $newUserMail;
    // 	$user->save();
    // 	return 1;
    // }    


    private static function changePwd ( $oldEmail , $newUserPwd ) {

    	$user = User::where('email',$oldEmail)->get()->first();

    	if( $user->password == md5 ( $newUserPwd ) ) { return 0; }

    	$user->password = md5( $newUserPwd );
    	$user->save();
    	return 1;

    }

}
