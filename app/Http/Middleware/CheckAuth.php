<?php

namespace App\Http\Middleware;


use Closure;
use JWTAuth;
use App\Providers\responseProvider;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Session as Session;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $r = new responseProvider();

        if( empty( $request->token ) ){
            $r->inError(1);
            return response()->json( $r->getResponseData() ); 
        }   

        $session = Session::where('token', $request->token)->get()->first();
        
        if( !empty($session)  ){
            if( $session->validUntil < date('Y-m-d H:i:s') ){
                $r->inError(3);
                return response()->json( $r->getResponseData() );
            }
            
        }else{
            $r->inError(2);
            return response()->json( $r->getResponseData() );
        }

    
        return $next($request);
    }
}
