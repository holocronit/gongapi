<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Crypt;

class responseProvider extends ServiceProvider
{

    public $result              = TRUE;
    public $error               = FALSE;
    public $token               = FALSE;
    public $token_data          = FALSE;
    public $statusPwd           = FALSE;
    public $statusEmail         = FALSE;

    private $crypt              = FALSE;

    public $data                = array();
    private $errorMsg           = array();
    private $rightMsg           = array();


    CONST TOKEN_KEY             = 'token';
    CONST ERROR_KEY             = 'error';
    CONST RESULT_KEY            = 'result';
    CONST STATUS_EMAIL_KEY      = 'statusEmail';
    CONST STATUS_PWD_KEY        = 'statusPwd';
    CONST DEFAULT_ERROR_MSG     = 'Errore indefinito';

    CONST EXPIRES_TIME          = '1970-01-01 00:00:00';


    public function __construct()
    {
        $errorJsonMsg               = file_get_contents( config_path().'/errorDescriptionList.json' );
        $this->errorMsg             = json_decode( $errorJsonMsg );

    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        error_log('boot-response');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        error_log('register-response');
    }


    public function addData( $data = FALSE , $key = FALSE )
    {
        if( $data ){
            if($key){
                $this->data[ $key ] = $data;
            }else{
                if(is_array($data)){
                     $this->data = array_merge( $this->data , $data );
                }
            }
        }
    }

    public function generate_token ( $user_id , $expire_time = self::EXPIRES_TIME  , $extra_data = array()) {

        if( empty( $user_id )  ) return '';

        $token_data                 = array();
        $token_data['id']           = intval($user_id);
        $token_data['expireDate']   = $expire_time;
        $token_data                 = array_merge( $token_data , $extra_data );

        $token_string   = json_encode( $token_data );

        $token = Crypt::encrypt( $token_string );

        $this->token        = $token;
        $this->token_data   = $token_data;

        return $token;        


    }


    public function read_token( $token = FALSE ){

        if( !$token || empty($token)) return FALSE;

        // Token Decryption
        $token_data = Crypt::decrypt( $token );
        
        // Read Token
        if ( $token_data ) $token_data = json_decode( $token_data , TRUE );

        $this->token        = $token;
        $this->token_data   = $token_data;

        return $token_data;

    }




    public function addToken( $token = FALSE )
    {
        if( !empty($token) ){
            $this->token = trim($token);
        }
    }


    public function getResponseData()
    {
        
        $response = array();

        $response[ $this::RESULT_KEY ] = $this->result;
        if( $this->error ){
            $response[ $this::ERROR_KEY ] = $this->error;
        } 


        if( $this->token ){
            $response[ $this::TOKEN_KEY ] = $this->token;
        } 

        $response = array_merge( $response , $this->data );

        return $response;

    }


    public function getUpdateData()
    {
        
        $response = array();

        $response[ $this::RESULT_KEY ] = $this->result;
        
        if( $this->error ){
            $response[ $this::ERROR_KEY ] = $this->error;
        } 


        if( $this->statusPwd ){
            $response[ $this::STATUS_PWD_KEY ] = $this->statusPwd;
        }

        if( $this->statusEmail ){
            $response[ $this::STATUS_EMAIL_KEY ] = $this->statusEmail;
        } 

        $response = array_merge( $response , $this->data );

        return $response;

    }




    public function pwdUpdated( $changeEff ) {

        if ( !$changeEff ) {

            $rightMsg = "La password non è stata cambiata";

            $this->statusPwd = [ 'status' => 'OK', 'desc' => $rightMsg ];

        } else {

            $rightMsg = "La password è stata cambiata";

            $this->statusPwd = [ 'status' => 'OK', 'desc' => $rightMsg ];
        } 

    }

    // public function emailUpdated( $changeEff ) {

    //     if ( !$changeEff ) {

    //         $rightMsg = "L'email non è stata cambiata";

    //         $this->statusEmail = [ 'status' => 'OK', 'desc' => $rightMsg ];

    //     } else {

    //         $rightMsg = "L'email è stata cambiata";

    //         $this->statusEmail = [ 'status' => 'OK', 'desc' => $rightMsg ];

    //     } 

    // }


    public function inError( $errorCode = -1 )
    {   

        $this->result = FALSE;

        $errorDesc  = $this->errorMsg->$errorCode;

        $errorDesc .= ' - #'.str_pad( $errorCode , 6, 0 , STR_PAD_LEFT  );

        // $errorDesc = isset( $this->errorMsg[$errorCode] ) ? $this->errorMsg[$errorCode] : $this::DEFAULT_ERROR_MSG.' - #'.str_pad( $errorCode , 6, 0 , STR_PAD_LEFT  );

        $this->error = [ 'code' => $errorCode , 'desc' => $errorDesc  ];
    }




}
