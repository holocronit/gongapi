<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    
    protected $table = 'sessions';


	public function user(){
		return $this->belongsTo('App\User','id');

    }

}
