<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
    protected $table = 'users';
    protected $hidden = ['password'];

    public function capabilities(){
        return $this->belongsTo('App\Caps', 'user_id');
    }

    public function role(){
    	return $this->hasOne('App\Role', 'user_id');
    }

    public function session(){
    	return $this->hasMany('App\Session', 'user_id');
    }

}
