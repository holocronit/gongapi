<?php

use Illuminate\Database\Seeder;

class CapsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('caps')->delete();

	 	DB::table('caps')->insert([
            'user_id' => '1',
            'service_id' => '1',
            'valid_until' => date('Y-m-d H:i:s')
            ]);

	 	DB::table('caps')->insert([
            'user_id' => '2',
            'service_id' => '1',
            'valid_until' => date('Y-m-d H:i:s')
            ]);	

	 	DB::table('caps')->insert([
            'user_id' => '1',
            'service_id' => '2',
            'valid_until' => date('Y-m-d H:i:s')
            ]);
    }
}
