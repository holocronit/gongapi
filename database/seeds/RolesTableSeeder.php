<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('roles')->delete();

	 	DB::table('roles')->insert([
            'user_id' => '1',
            'roleName' => 'Admin'
            ]);

	 	DB::table('roles')->insert([
            'user_id' => '2',
            'roleName' => 'User'
            ]);
    }
}
