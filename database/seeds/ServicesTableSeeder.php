<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('services')->delete();

	 	DB::table('services')->insert([
            'name' => 'RealTime Monitor',
            'slug' => 'real_time_monitor',
            'local_path' => '#',
            'public_path' => '#'
            ]);

	 	DB::table('services')->insert([
            'name' => 'Server Tracker',
            'slug' => 'server_tracker',
            'local_path' => '#',
            'public_path' => '#'
            ]);

	 	DB::table('services')->insert([
            'name' => 'Query Logger',
            'slug' => 'query_logger',
            'local_path' => '#',
            'public_path' => '#'
            ]);
    }
}
