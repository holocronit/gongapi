 <?php


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
//use DB;


 class UserTableSeeder extends Seeder {
 /** * Run the database seeds.

 * * @return void */ 

	public function run() {

	 	DB::table('users')->delete();

	 	DB::table('users')->insert([
            'email' => 'admin@holocron.it',
            'password' => md5('admin'),
            'role_id' => 1,
            'name' => 'Admin',
            'surname' => 'Holocron',
            'company' => 'Holocron',
            'role_company' => 'CEO',
            'fiscal_code' => 'hlcdmn92c29g702u',
            'vat' => '01234567891',
            'mobile' => '3331122333',
            'phone' => '050500525',
            'address' => 'Via Crispi 25/27',
            'city' => 'Pisa',
            'state' => 'Italy'
            ]);

	 	DB::table('users')->insert([
            'email' => 'user@holocron.it',
            'password' => md5('user'),
            'role_id' => 2,
            'name' => 'User',
            'surname' => 'Holocron',
            'company' => 'Holocron',
            'role_company' => 'Web Developer',
            'fiscal_code' => 'hlcsru93c17g702u',
            'vat' => '01234567891',
            'mobile' => '3334455667',
            'phone' => '050500525',
            'address' => 'Via Crispi 25/27',
            'city' => 'Pisa',
            'state' => 'Italy'
            ]);

 	}
}