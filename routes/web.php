<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 *
 * Rotta per effettuare la login
 * ErrorBase: 100-109
 */
Route::get('/', function () {
    return view('welcome');
});




/**
 *
 * Rotta per effettuare la login
 * ErrorBase: 100-109
 */
Route::post('/login', 'AuthenticateController@authenticate' );

/**
 *
 * Rotta per effettuare la logout
 * ErrorBase: 110-119
 */
Route::post('/logout', 'AuthenticateController@deauthenticate' )->middleware('strongAuth');

/**
 *
 * Rotta per la gestione profilo
 * ErrorBase: 120-129
 */

Route::post('/profile/changePassword', 'ChangePasswordController@update' )->middleware('strongAuth');;